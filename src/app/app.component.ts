import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExportPdfService } from './services/pdf-service/export-pdf.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public j = 0;
  public studentList;
  public titleList;
  public data = new Array();
  public workbook: XLSX.WorkBook;
  public sheetlists = new Array();
  public fontName: string;
  public filteredOptions; // Filtered Fonts List
  public salutationss = true;
  public setPosition: string = 'false'; // set the positon of the fonts
  public selectedExcel; // excel sheets
  public selectedSheet; // sheets in the excel file
  public certificateHeight: number;
  public certificateWidth: number; // certificate hight width
  public cancleResize: boolean = false;
  public imageHeight;
  public imageWidth;
  public fontSizes = [
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66,
    67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85,
    86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
  ];
  public showSign: boolean = false; // for Showing and hiding signature
  public showLogo: boolean = false;
  public colorbutton: boolean = true; // for color disable number
  public isLoading: boolean = false; // loader variable
  public url; // image url
  public signature; // signature
  public logos;
  public rotation = 0;
  public update;

  // defining form Group
  public myForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private _snackBar: MatSnackBar,
    private exportPdfService: ExportPdfService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.myForm = this.fb.group({
      image: ['', Validators.required],
      excel: [''],
      fields: this.fb.array([]),
    });
    this.myForm.get('fields').valueChanges.subscribe((response) => {
      document.getElementById('f1').textContent = response[0];
      document.getElementById('f2').textContent = response[1];
      document.getElementById('f3').textContent = response[2];
      document.getElementById('f4').textContent = response[3];
      document.getElementById('f5').textContent = response[4];
    });
  }

  get addFields() {
    return this.myForm.get('fields') as FormArray;
  }

  addField() {
    if (this.myForm.get('image').value !== '') {
      if (this.addFields.length < 5) {
        this.addFields.push(this.fb.control(''));
      }
      this.addfield(this.addFields);
    } else {
      this.openSnackBar('Upload Certificate', 'X', 'reds');
    }
  }

  addfield(array) {
    for (let index = 0; index < array.length; index++) {
      const element = array;
    }
  }

  deleteFields(i) {
    if (this.addFields) {
      if (this.addFields.length < 2) {
      }
      this.addFields.removeAt(i);
    }
  }

  onFileChange(evt: any) {
    // excel file
    const target: DataTransfer = evt.target as DataTransfer;
    this.selectedExcel = target;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      // read workbook
      const bstr: string = e.target.result;
      this.workbook = XLSX.read(bstr, { type: 'binary' });
      // grab the sheet list
      this.sheetlists = this.workbook.SheetNames;
      this.selectedSheet = '0';
      /* grab first sheet */
      const wsname: string = this.workbook.SheetNames[0];
      const ws: XLSX.WorkSheet = this.workbook.Sheets[wsname];
      this.data = XLSX.utils.sheet_to_json(ws, { header: 0 });
      this.selectChange();
    };
  }

  // selectedSheet
  selectChange() {
    if (this.myForm.get('image').value !== '') {
      if (this.selectedExcel.files.length === 1) {
        const wsname: string = this.workbook.SheetNames[this.selectedSheet];
        const ws: XLSX.WorkSheet = this.workbook.Sheets[wsname];
        this.data = XLSX.utils.sheet_to_json(ws, { header: 0 });
        this.data = this.data.map((value) => {
          return {
            ...value,
            Salu:
              value.Gender.includes('M') ||
              value.Gender.includes('m') ||
              value.Gender.includes('Male') ||
              value.Gender.includes('male')
                ? 'Mr. '
                : value.Gender.includes('F') ||
                  value.Gender.includes('f') ||
                  value.Gender.includes('Female') ||
                  value.Gender.includes('female')
                ? 'Ms. '
                : '',
          };
        });
        this.studentList = this.data.map((value) => {
          return [value.Name];
        });
        this.titleList = this.data.map((value) => {
          return [value.Salu];
        });
        if (this.salutationss) {
          document.getElementById('text').textContent =
            this.titleList[this.j] + this.studentList[this.j];
        } else {
          document.getElementById('text').textContent =
            this.studentList[this.j];
        }
      } else {
        this.openSnackBar('Excel file missing', 'X', 'reds');
        document.getElementById('text').textContent = ` `;
      }
    } else {
      this.openSnackBar('Upload Certificate', 'X', 'reds');
    }
  }

  // to load an image in the view
  selectFile(event) {
    if (event.target.files) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (ev: any) => {
        this.url = ev.target.result;
      };
      document.getElementById('texts').textContent = ``;
    }
  }

  // to rotate the image
  rotate() {
    if (this.myForm.get('image').value !== '') {
      document.getElementById('rotate').style.background = 'cadetblue';
      this.rotation += 90; // add 90 degrees, you can change this as you want
      if (this.rotation === 360) {
        document.getElementById('rotate').style.background =
          'linear-gradient(to right top, #65dfc9, #6cdbeb)';
        this.rotation = 0;
      }
      document.getElementById(
        'parent'
      ).style.transform = `rotate(${this.rotation}deg)`;
    } else {
      this.openSnackBar('Upload Certificate', 'X', 'reds');
    }
  }

  setposition() {
    const position = document.getElementById('position');
    this.setPosition === 'false'
      ? (this.setPosition = 'true')
      : (this.setPosition = 'false');
    if (this.setPosition === 'true') {
      position.style.background = 'cadetblue';
    } else {
      position.style.background =
        'linear-gradient(to right top, #65dfc9, #6cdbeb)';
    }
  }

  resize(height, width) {
    const img = document.getElementById('certificate');
    this.imageHeight = img.offsetHeight;
    this.imageWidth = img.offsetWidth;

    if (this.myForm.get('image').value !== '') {
      if (height !== '' || width !== '') {
        this.cancleResize = !this.cancleResize;
        const image = document.getElementById('certificate');
        image.style.height = height + 'px';
        image.style.width = width + 'px';
      } else {
        this.openSnackBar('Field value missing', 'X', 'reds');
      }
    } else {
      this.openSnackBar('Certificate is missing', 'X', 'reds');
    }
  }

  cancelResize() {
    const image = document.getElementById('certificate');
    image.style.height = this.imageHeight + 'px';
    image.style.width = this.imageWidth + 'px';
    this.cancleResize = !this.cancleResize;
  }

  color(colors) {
    const color = document.getElementById(this.update);
    if (typeof this.update === 'undefined') {
      this.openSnackBar('Select the field first', 'X', 'reds');
    } else {
      colors === ''
        ? this.openSnackBar('The color field is empty', 'X', 'reds')
        : colors.length === 6
        ? (color.style.color = `#${colors}`)
        : this.openSnackBar('The color is invalid', 'X', 'reds');
    }
  }

  exportss(pdf) {
    document.getElementById(this.update).classList.remove('active');
    if (this.myForm.get('excel').value !== '') {
      this.exp(pdf);
    } else {
      this.spinner.show();
      document.getElementById('singlePdf').click();
      setTimeout(() => {
        this.spinner.hide();
        this.openSnackBar('Download Complete', 'X', 'greens');
      }, 5000);
    }
  }

  exp(pdf) {
    if (this.j < this.data.length && this.studentList[this.j]) {
      this.spinner.show();
      if (this.salutationss) {
        this.exportPdfService
          .export(pdf, this.titleList[this.j] + this.studentList[this.j])
          .then(() => {
            this.j++;
            document.getElementById('text').textContent =
              this.titleList[this.j] + this.studentList[this.j];
            setTimeout(() => {
              const downloadButton = document.getElementById('pdfExport');
              downloadButton.click();
            }, 100);
          });
      } else {
        this.exportPdfService.export(pdf, this.studentList[this.j]).then(() => {
          this.j++;
          document.getElementById('text').textContent =
            this.studentList[this.j];
          setTimeout(() => {
            const downloadButton = document.getElementById('pdfExport');
            downloadButton.click();
          }, 100);
        });
      }
    } else {
      this.spinner.hide();
      this.j = 0;
      this.salutationss
        ? (document.getElementById('text').textContent =
            this.titleList[this.j] + this.studentList[this.j])
        : (document.getElementById('text').textContent =
            this.studentList[this.j]);
      this.openSnackBar('Download Complete', 'X', 'greens');
    }
  }

  selectFont(event) {
    const path = event.target.files[0];
    this.fontName = path.name.substr(0, 5);
    const blop = URL.createObjectURL(path);
    const style = document.createElement('style');
    style.innerHTML = `@font-face {font-family: ${this.fontName};src: url("${blop}") format("truetype");}`;
    document.head.appendChild(style);
  }

  changeFont() {
    if (typeof this.update === 'undefined') {
      this.openSnackBar('Select the field first', 'X', 'reds');
    } else {
      document.getElementById(this.update).style.fontFamily = this.fontName;
    }
  }

  salutation() {
    if (
      typeof this.selectedExcel === 'undefined' ||
      typeof this.selectedSheet === 'undefined'
    ) {
      this.openSnackBar('Excel file or Excel Sheet Missing', 'X', 'reds');
    } else {
      this.salutationss = !this.salutationss;
      if (this.salutationss) {
        document.getElementById(
          'text'
        ).textContent = `${this.data[0].Salu} ${this.data[0].Name}`;
      } else {
        document.getElementById('text').textContent = ` ${this.data[0].Name}`;
      }
    }
  }

  updateFont(font) {
    if (typeof this.update === 'undefined') {
      this.openSnackBar('Select the field first', 'X', 'reds');
    } else {
      document.getElementById(this.update).style.fontFamily = font;
    }
  }

  selectSignature(event) {
    if (event.target.files) {
      this.showSign = true;
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (ev: any) => {
        this.signature = ev.target.result;
      };
    }
  }

  selectLogo(events) {
    if (events.target.files) {
      this.showLogo = true;
      const reader = new FileReader();
      reader.readAsDataURL(events.target.files[0]);
      reader.onload = (ev: any) => {
        this.logos = ev.target.result;
      };
    }
  }

  showsign(val) {
    val.length !== 0
      ? (this.showSign = !this.showSign)
      : this.openSnackBar(
          'Please upload the signature/logo first',
          'X',
          'reds'
        );
  }

  showlogo(val) {
    val.length !== 0
      ? (this.showLogo = !this.showLogo)
      : this.openSnackBar(
          'Please upload the signature/logo first',
          'X',
          'reds'
        );
  }

  openSnackBar(x, y, className) {
    this._snackBar.open(x, y, {
      horizontalPosition: 'right',
      verticalPosition: 'top',
      duration: 2000,
      panelClass: [className],
    });
  }

  onClick(event, a = null) {
    const buttons = document.querySelectorAll('p');
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    this.update = value;
    buttons.forEach((btn) => btn.classList.remove('active'));
    document.getElementById(this.update).classList.add('active');
  }

  fontsizes(fontsize) {
    if (typeof this.update === 'undefined') {
      this.openSnackBar('Select the field first', 'X', 'reds');
    } else {
      document.getElementById(this.update).style.fontSize = fontsize + 'px';
    }
  }
}
