import { Injectable } from '@angular/core';
import { exportPDF, Group } from '@progress/kendo-drawing';
@Injectable({
  providedIn: 'root',
})
export class ExportPdfService {
  public base64: string;
  public fileBase: any;
  public objectURl: any;
  public blobObject: any;
  public a = 0;

  constructor() {}

  public export(pdfComponent: any, fileName: string) {
    const promise = new Promise<void>((resolve, reject) => {
      pdfComponent
        .export()
        .then((group: Group) => exportPDF(group))
        .then((dataUri) => {
          this.base64 = dataUri.replace('data:application/pdf;base64,', '');
          this.fileBase = this.dataURLtoFile(dataUri, fileName + '.pdf').then(
            (data) => {
              this.objectURl = window.URL.createObjectURL(this.blobObject);
              const a = document.createElement('a');
              a.href = this.objectURl;
              a.download = fileName + '.pdf';
              a.click();
              resolve();
            }
          );
        });
    });
    return promise;
  }

  public dataURLtoFile(dataurl, filename) {
    return new Promise((resolve, reject) => {
      const arr = dataurl.split(',');
      const mime = arr[0].match(/:(.*?);/)[1];
      const bstr = atob(arr[1]);
      let n = bstr.length;
      const u8arr = new Uint8Array(n);
      while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
      }
      this.blobObject = new Blob([u8arr], { type: mime });
      this.objectURl = window.URL.createObjectURL(this.blobObject);
      resolve(new File([u8arr], filename, { type: mime }));
    });
  }
}
